use std::panic;

mod panik;

pub fn install() {
    let previous_hook = panic::take_hook();

    panic::set_hook(Box::new(move |info| {
        previous_hook(info);
        eprintln!("{}", panik::PANIK);
    }));
}


#[test]
fn panic() {
    install();

    panic!("AAA")
}
